# Go-2-Trip-Client

### Overview
Go2Trip is a website for selling trip products online. The website allows users to sell trip products they no longer use.

### objective
I created this website to meet the needs of travelers in my country, where the options available are limited. Department stores offer travel items, but their quality is poor and prices are extreme. My main goal of this website is to offer a selection of travel products.

## Prerequisites
Before you begin, ensure you have met the following requirements:

- Node.js and npm installed on your machine.
- Git installed on your machine.

## Getting Started

To get a local copy up and running, follow these simple steps.

1. Clone the repository:
    ```bash
    git clone https://github.com/adnankhayrou/Go-2-Trip-Client.git
    ```
3. Install dependencies using npm:
    ```bash
    npm install
    ```

## Available Scripts
In the project directory, you can run:

### npm run dev
The App Run on Port 5173


## Additional Dependencies

The following additional dependencies are part of the project:

1. **axios**
   - **Version**: ^1.5.1
   - **Description**: A promise-based HTTP client used for making asynchronous requests in the project.

2. **js-cookie**
   - **Version**: ^3.0.5
   - **Description**: A JavaScript library for simple, lightweight handling of browser cookies.

3. **react**
   - **Version**: ^18.2.0
   - **Description**: A JavaScript library for building user interfaces.

4. **react-dom**
   - **Version**: ^18.2.0
   - **Description**: Provides DOM-specific methods used by React.

5. **react-router-dom**
   - **Version**: ^6.17.0
   - **Description**: Declarative routing for React.

6. **tailwindcss**
   - **Version**: ^3.3.3
   - **Description**: A CSS framework for rapidly building custom designs.

7. **vite**
   - **Version**: ^4.4.5
   - **Description**: A fast build tool that significantly improves frontend development experience.

8. **sweetalert2**
   "Version": "^11.10.1",
   - **Description**: SweetAlert is a JavaScript library that provides a simple and elegant way to create alert dialogs in web applications.

9. **yup**
   "Version": "^1.3.2",
   - **Description**: Object schema validator for JavaScript.

10. **date-fns**
   "Version": "^3.3.1",
   - **Description**: A package to change date format.


### project-structure
- **directories**:

- `Components/assets` : Static assets such as images, svg.
- `src` : Contains folders for Components, Routes.
- `Components` : Contains component for different routes and actions.
- `App.jsx` : Contains routes for different component of the application.
- `package.json` : Contains dependencies of the application.
